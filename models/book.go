package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Book struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name      string             `json:"name,omitempty" bson:"name,omitempty"`
	Author    string             `json:"author,omitempty" bson:"author,omitempty"`
	Year      int                `json:"year,omitempty" bson:"year,omitempty"`
	ISBN      string             `json:"isbn,omitempty" bson:"isbn,omitempty"`
	CreatedAt time.Time          `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAt time.Time          `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
}

type BookServiceError struct {
	ErrorMsg string `json:"error,omitempty" bson:"error,omitempty"`
}
