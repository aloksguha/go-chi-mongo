FROM golang:latest

ENV GO111MODULE=on

RUN mkdir -p /go/src/go-chi-mongo

RUN mkdir -p /go/bin

WORKDIR /go/src/go-chi-mongo

COPY . /go/src/go-chi-mongo

RUN go mod download

RUN go build -o /go/bin/bookserver .

RUN rm -rf /go/src

EXPOSE 3333

CMD ["/go/bin/bookserver"]