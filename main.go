package main

import (
	"go-chi-mongo/controllers"
	"go-chi-mongo/db"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"net/http"
)

func main() {
	db.InitDB()
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Post("/book", controllers.CreateBook)
	r.Get("/books", controllers.GetAllBook)
	r.Get("/book/{id}", controllers.GetBook)
	r.Put("/book/{id}", controllers.UpdateBook)
	r.Delete("/book/{id}", controllers.DeleteBook)

	_ = http.ListenAndServe(":3333", r)
}
