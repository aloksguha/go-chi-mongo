package controllers

import (
	"go-chi-mongo/db"
	"go-chi-mongo/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"os"
	"reflect"
	"time"
)

func CreateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var book models.Book
	decodeError := json.NewDecoder(r.Body).Decode(&book)
	book.CreatedAt = time.Now()
	book.UpdatedAt = time.Now()
	if decodeError != nil {
		panic(decodeError)
	}
	collection := db.Client.Database("bookdb").Collection("book")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	insertResult, err := collection.InsertOne(ctx, book)

	if err != nil {
		log.Fatal(err)
	}
	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(insertResult.InsertedID)
}

func GetAllBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var books []models.Book
	collection := db.Client.Database("bookdb").Collection("book")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var book models.Book
		cursor.Decode(&book)
		books = append(books, book)
	}
	if len(books) > 0 {
		_ = json.NewEncoder(w).Encode(books)
	} else {
		w.WriteHeader(http.StatusNoContent)
		_ = json.NewEncoder(w).Encode(models.BookServiceError{"Book not found"})
	}

}

func GetBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var book models.Book
	id := chi.URLParam(r, "id")
	fmt.Print("\nGetting Book of Id : " + id + "\n")
	collection := db.Client.Database("bookdb").Collection("book")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	objID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": bson.M{"$eq": objID}} //bson.M{"_id": objID}
	result := collection.FindOne(ctx, filter)
	result.Decode(&book)
	if book.ID != objID {
		w.WriteHeader(http.StatusNoContent)
		_ = json.NewEncoder(w).Encode(models.BookServiceError{"Book not found"})
	} else {
		_ = json.NewEncoder(w).Encode(book)
	}
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	id := chi.URLParam(r, "id")
	fmt.Print(id)
	collection := db.Client.Database("bookdb").Collection("book")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	objID, _ := primitive.ObjectIDFromHex(id)
	result, err := collection.DeleteOne(ctx, bson.M{"_id": objID})
	if err != nil {
		log.Fatal(err)
	}
	_ = json.NewEncoder(w).Encode(result)
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	id := chi.URLParam(r, "id")
	objID, _ := primitive.ObjectIDFromHex(id)
	fmt.Print(id + "\n")
	filterToFindRecord := bson.M{"_id": objID}
	var book models.Book
	decondeError := json.NewDecoder(r.Body).Decode(&book)
	if decondeError != nil {
		panic(decondeError)
	}
	book.ID = primitive.NilObjectID

	collection := db.Client.Database("bookdb").Collection("book")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	update := bson.M{"$set": bson.M{
		"name":      book.Name,
		"author":    book.Author,
		"isbn":      book.ISBN,
		"updatedAt": time.Now(),
	}}
	result, err := collection.UpdateOne(ctx, filterToFindRecord, update)
	if err != nil {
		fmt.Println("UpdateOne() result ERROR:", err)
		os.Exit(1)
	} else {
		fmt.Println("UpdateOne() result:", result)
		fmt.Println("UpdateOne() result TYPE:", reflect.TypeOf(result))
		fmt.Println("UpdateOne() result MatchedCount:", result.MatchedCount)
		fmt.Println("UpdateOne() result ModifiedCount:", result.ModifiedCount)
		fmt.Println("UpdateOne() result UpsertedCount:", result.UpsertedCount)
		fmt.Println("UpdateOne() result UpsertedID:", result.UpsertedID)
	}

	if result.ModifiedCount > 0 {
		collection.Find(ctx, filterToFindRecord)
		_ = json.NewEncoder(w).Encode(book)
	} else {
		var msg = "Book not found"
		_ = json.NewEncoder(w).Encode(models.BookServiceError{msg})
	}
}
